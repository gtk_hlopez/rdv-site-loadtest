import { Utils } from '../utils/utils';

let clientList = [
	{ name: 'Voronezh Marriott', clientID: 10221},
	{ name: 'Courtyard Warsaw Airport', clientID: 9395},
	{ name: 'Mandarin Oriental Dubai', clientID: 9991},
	{ name: 'Renaissance Warsaw Airport', clientID: 10303},
	{ name: 'Ritz-Carlton Vienna', clientID: 7986},
	{ name: 'York Marriott', clientID: 7694},
	{ name: 'Worsley Park Marriott Hotel and Country Club', clientID: 7388},
	{ name: 'Waltham Abbey Marriott', clientID: 7326},
	{ name: 'Tudor Park Marriott Hoteland Country Club', clientID: 7918}
];

Utils.testRDVSites(clientList);