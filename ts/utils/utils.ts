// import { Config } from '../../env';

export class Utils {

	public static testRDVSites(sites) {
		sites.forEach((value) => {
				it('Testing page load for site: ' + value.name, () => {
					cy.clearCookies();
					cy.clearLocalStorage();
					cy.request('https://rdv-' + value.clientID + '.gtkserver.net/').then((response) => {
						expect(response.status).to.eq(200);
					});
				});
		});
	} 
}