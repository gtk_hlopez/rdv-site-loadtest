# Site loader

This project uses Cypress to check if RDV sites load.


### Installation

Install [nodejs](https://nodejs.org/en/download/). If you're using windows, install [Git Bash](https://git-scm.com/downloads). 


Once down, clone the project and install the dependencies.

```sh
$ git clone https://gtk_hlopez@bitbucket.org/gtk_hlopez/rdv-site-loadtest.git
$ cd rdv-siteloadtest/
$ npm install
```

This should install all the dependencies required for this automation project.
As this project is implemented in typescript, make sure the typescript CLI is running by checking it's version after rebooting your terminal.

```sh
$ tsc -v
Version 2.8.3
```

If you get `bash:tsc:command not found` error, just run the following command and check once again:

```sh
$ npm install -g typescript
```

### Run

To run Cypress portal, run the following command:

```sh
$ npm start
```

To run a headless automation , run the following command:

```sh
$ npm run headless
``` 

To run a headless automation check on all sites, run the following command:

```sh
$ npm run headless-all
``` 

To run a headless automation check on qa sites, run the following command:

```sh
$ npm run headless-qa
``` 
To run a headless automation check on asia sites, run the following command:

```sh
$ npm run headless-asia
``` 
To run a headless automation check on america sites, run the following command:

```sh
$ npm run headless-americas
``` 
To run a headless automation check on emea sites, run the following command:

```sh
$ npm run headless-emea
``` 
To run a headless automation check on first 10 emea sites to be deployed, run the following command:

```sh
$ npm run headless-emea-eastern
``` 

To run a headless automation check on the Americas sites for 2019 , run the following command

```sh
$ npm run headless-americas-2019
```

To run a headless automation check on the Americas last 3 sites , run the following command

```sh
$ npm run headless-americas-3
```

This should compile all typescript code into js/ folder and then run the tests.



### NOTE

 - DO NOT commit the node_modules, cypress folder to the repository. 
 - To add new headless tests modify the package.json file



