"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils/utils");
let clientList = [
    { name: 'Atlanta Marriott Marquis', clientID: 5081 },
    { name: 'Atlanta Marriott Suites Midtown', clientID: 39 },
    { name: 'Baltimore Marriott Waterfront', clientID: 481 },
    { name: 'Chicago Marriott Naperville', clientID: 9713 },
    { name: 'Conrad New York', clientID: 723 },
    { name: 'Coronado Island Marriott Resort & Spa', clientID: 7782 },
    { name: 'Courtyard Chicago Downtown River North', clientID: 8742 },
    { name: 'Fort Lauderdale Marriott Harbor Beach Resort & Spa', clientID: 5157 }
];
utils_1.Utils.testRDVSites(clientList);
