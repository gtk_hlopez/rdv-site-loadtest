"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils/utils");
let clientList = [
    { name: 'Hyatt Regency Sonoma Wine Country', clientID: 3125 },
    { name: 'Las Vegas Marriott', clientID: 7212 },
    { name: 'St. Petersburg Marriott Clearwater', clientID: 9134 }
];
utils_1.Utils.testRDVSites(clientList);