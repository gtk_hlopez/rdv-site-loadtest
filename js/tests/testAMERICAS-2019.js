"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils/utils");
let clientList = [
    { name: 'Anaheim Marriot Suites', clientID: 4043 },
    { name: 'Courtyard Kansas City', clientID: 6315 },
    { name: 'Courtyard Lake', clientID: 9196 },
    { name: 'JW Marriott Nashville', clientID: 10443 },
    { name: 'Key Bridge Marriott', clientID: 8964 },
    { name: 'Tampa Marriott Waterside', clientID: 33 },
    { name: 'W New york Times Square', clientID: 4965 },
];
utils_1.Utils.testRDVSites(clientList);
