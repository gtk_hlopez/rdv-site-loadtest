"use strict";
// import { Config } from '../../env';
Object.defineProperty(exports, "__esModule", { value: true });
class Utils {
    static testRDVSites(sites) {
        sites.forEach((value) => {
            it('Testing page load for site: ' + value.name, () => {
                cy.clearCookies();
                cy.clearLocalStorage();
                cy.request('https://rdv-' + value.clientID + '.gtkserver.net/').then((response) => {
                    expect(response.status).to.eq(200);
                });
            });
        });
    }
}
exports.Utils = Utils;
